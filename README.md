# proteomics_variable_selection

Tools in R for doing variable selection with mass spec data. 

Basic pipeline is
1. first run pipeline_combine.R 
  - this will combine results across multiple surfaces and output files proteome.txt and proteome_attr.txt, which contain the merged intensity data
  
 2. the run pipeline_select.R 
 
 At the moment the scripts have hardcoded paths and are specific for analysis done for Shea Hamilton late 2017/early 2018